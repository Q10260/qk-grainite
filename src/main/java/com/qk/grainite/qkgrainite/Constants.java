package com.qk.grainite.qkgrainite;

public class Constants {
  public static String APP_NAME = "qk-grainiteApp";

  public static String LINE_TOPIC = "line_topic";

  public static final String LINE_TABLE = "line_table";
  public static final String HANDLE_LINE_EVENT_ACTION = "handleLineEvent";

  public static final String DOC_STATS_TABLE = "doc_stats_table";
  public static final String HANDLE_DOC_STATS_EVENT = "handleDocStatsEvent";

  public static final String WORDS_TABLE = "word_stats_table";
  public static final String HANDLE_WORD_EVENTS_ACTION = "handleWordEvent";
}
