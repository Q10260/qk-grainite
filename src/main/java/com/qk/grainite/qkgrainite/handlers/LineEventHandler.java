package com.qk.grainite.qkgrainite.handlers;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.TopicEvent;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;

public class LineEventHandler {
	
	public ActionResult handleLineEvent(Action action, GrainContext context) {
	    LineEvent event = ((TopicEvent) action).getPayload().asType(LineEvent.class);

	    final AtomicLong wordCount = new AtomicLong(0);
	    Stream.of(event.getLine().split("[. ]+")).forEach(w -> {
	      // for each word, send to word stats table, and increment document count.
	      if (w.trim().length() == 0) return;

	      w = w.toLowerCase();
	      wordCount.incrementAndGet();
	    });

	    return ActionResult.success(action);
	  }
	
	
	public static class LineEvent implements Serializable {
	    private String docName;
	    private String line;

	    public LineEvent() {}

	    public LineEvent(String docName, String line) {
	      this.docName = docName;
	      this.line = line;
	    }

	    public String getLine() {
	      return line;
	    }

	    public String getDocName() {
	      return docName;
	    }

	    @Override
	    public String toString() {
	      return "LineEvent{"
	          + "docName='" + docName + '\'' + ", line='" + line + '\'' + '}';
	    }
	  }
}
