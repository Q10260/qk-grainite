package com.qk.grainite.qkgrainite.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import com.grainite.api.Callback;
import com.grainite.api.Event;
import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.GrainiteException;
import com.grainite.api.Key;
import com.grainite.api.KeyValue;
import com.grainite.api.MapQuery;
import com.grainite.api.RecordMetadata;
import com.grainite.api.Table;
import com.grainite.api.Topic;
import com.grainite.api.Value;
import com.grainite.api.GrainiteException.ErrorType;
import com.qk.grainite.qkgrainite.Constants;

public class Test {

	public static void main(String[] args) throws IOException {

		Grainite client = GrainiteClient.getClient("localhost", 5056);
		String cmd = args[0];
		switch (cmd.toLowerCase()) {
		case "load":
			new Test().load(client, args[1]);
			break;
		case "word":
			new Test().getWordCount(client, args[1]);
			break;
		case "allwords":
			new Test().getAllWordsStats(client);
			break;
		}

	}

	private void getAllWordsStats(Grainite client) {
		List<String> allWordsList = new ArrayList<>();
		Table wordStats = client.getTable(Constants.APP_NAME, Constants.WORDS_TABLE);
		for (Iterator<Grain> iter = wordStats.scan(); iter.hasNext();) {
			Grain g = iter.next();

			// scan through all words in this bucket and print sorted
			Iterator<KeyValue> it = g.mapQuery(
					MapQuery.newBuilder().setMapId(0).setRange(Key.minKey(), true, Key.maxKey(), true).build());
			while (it.hasNext()) {
				KeyValue kv = it.next();
				allWordsList.add(String.format("%8d: %s", kv.getValue().asLong(), kv.getKey().asString()));
			}
		}
		allWordsList.stream().sorted((x, y) -> x.split(":")[1].compareTo(y.split(":")[1])).forEach(System.out::println);

	}

	private void getWordCount(Grainite client, String word) {
		Table wordTable = client.getTable(Constants.APP_NAME, Constants.WORDS_TABLE);
		Grain g = wordTable.getGrain(Value.of(word.substring(0, 1)), false);

		if (g == null) {
			System.out.printf("Word %s not found\n", word);
			System.exit(1);
		}

		try {
			System.out.printf("%s: %d\n", word, g.mapGet(0, Value.of(word)).asLong(0));
		} catch (GrainiteException ex) {
			if (ex.getErrorType() == ErrorType.ERRNO_KZ_NOEXIST) {
				System.err.printf("Word %s not found\n", word);
				System.exit(1);
			}

			ex.printStackTrace(System.err);
		}
	}

	private void load(Grainite client, String inputFile) throws IOException {
		Topic topic = client.getTopic(Constants.APP_NAME, Constants.LINE_TOPIC);
		Path inputPath = Paths.get(inputFile);
		AtomicBoolean didEncounterFailure = new AtomicBoolean(false);
		AtomicLong numEventsSent = new AtomicLong(0);
		AtomicLong numEventCallbacksReceived = new AtomicLong(0);
		System.out.println("Reading file and sending to the topic.");
		Files.lines(inputPath).forEach(line -> {
			if (!line.isEmpty()) {
				numEventsSent.incrementAndGet();
				topic.appendAsync(new Event(Value.of(line.substring(0, 2)), Value.of(line)),
						new Callback<RecordMetadata>() {
					@Override
					public void onCompletion(RecordMetadata completion, Exception exception) {
						numEventCallbacksReceived.incrementAndGet();
						if (exception != null) {
							didEncounterFailure.set(true);
						}
					}
				});
			}
		});

		topic.flush();
		while (numEventsSent.get() != numEventCallbacksReceived.get()) {
			// Wait for all events to be sent to the topic.
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (didEncounterFailure.get()) {
			System.err.println("[ERROR] Encountered an error while sending events to the topic.");
			System.exit(1);
		} else {
			System.out.println("[SUCCESS] File was read and all events were sent to the topic.");
		}
	}

}
